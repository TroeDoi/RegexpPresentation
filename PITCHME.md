# Regular expresions

#HSLIDE

#### Syntax

```
/pattern/flags
new RegExp(pattern[, flags])
```

#HSLIDE

#### Characters

| Character 	| Legend     	    |
| --------------|:-----------------:|
| **\w**        | Word character    |
| **\W**        | Non-word character|
| **\d** 		| Digit      		|
| **\D** 		| Non-digit  		|

#HSLIDE

#### More Characters

| Character 	| Legend     	   				  |
| --------------|:-------------------------------:|
| **.**         | Any character except line break |
| **\ **        | Escapes a special Character 	  |
| **\s** 		| Whitespace 					  |
| **\S** 		| Non-whitespace    			  |

#HSLIDE

#### More Whitespace

| Character 	| Legend     	   			|
| --------------|:-------------------------:|
| **\t**        | Tab 						|
| **\r**        | Carriage return character |
| **\n** 		| Line feed character		|
| **\r\n** 		| Line separator in windows	|
| **\v** 		| Vertical tab 				|


#HSLIDE

#### Quantifiers

| Quantifier 	| Legend       		 |
| --------------|:------------------:|
| **+** 		| One or more 		 |
| **{3}** 	 	| Exactly three times|   
| **{2,4}** 	| Two to four times  |    
| **{3,}**  	| Three or more times|    
| *****  		| Zero or more times |
| **?** 		| Once or none  	 |

#HSLIDE


Quantifiers are "greedy"

| Example 		| Match 			 |
| --------------|:------------------:|
| **\d+** 		| 1234567	 		 |
| **\w{2,4}** 	| abcd		 		 |

---

'**?**' Makes quantifiers "lazy"

| Example 		| Match 			 |
| --------------|:------------------:|
| **\d+?** 		| 1 in **1**234567	 |
| **\w{2,4}?** 	| ab in **ab**cd	 |

#HSLIDE

#### Logic

| Logic 		| Legend       			    |
| --------------|:-------------------------:|
| ** &#124; ** 	| Alternation / OR operand 	|
| **( ... )** 	| Capturing group			|   
| **\1** 		| Contents of Group 1  		|    
| **\2**  		| Contents of Group 2		|    
| **(?: ...)** 	| Non-capturing group 		|


#HSLIDE

#### Character classes

| Character		| Legend       			    							 |
| --------------|:------------------------------------------------------:|
| **[ ... ]** 	| One of the characters in the brackets 				 |
| **[ a-z ]** 	| One of the characters in the range from a to z		 |   
| **[^x]** 		| One character that is **not** x  						 |    
| **[^a-z]**  	| One of the characters **not** in the range from a to z |    


#HSLIDE

#### Anchors and Boundaries

| Anchor 		| Legend       			    									|
| --------------|:-------------------------------------------------------------:|
| **^** 		| Start of string or start of line depending on multiline mode	|
| **$** 		| End of string or end of line depending on multiline mode		|   
| **\b** 		| Word boundary  												|
| **\B**  		| **Not** a word boundary										|    


#HSLIDE

#### Lookarounds

| Anchor 		| Legend       			| Example    		| Match 				|
| --------------|:---------------------:|:-----------------:|:---------------------:|
| **(?=...)** 	| \+ lookahead	| \d+(?= dollars)  	| 100 in 100 dollars	|
| **(?!...)** 	| \- lookahead	| \d+(?!\d&#124; dollars)| 100 in 100 pesos	|
| **(?<=...)** 	| \+ lookbehind	| Not supported	| 		|
| **(?<!...)** 	| \- lookbehind	| Not supported |		|

#HSLIDE

#### Flags

| Flags 		| Legend       			    									|
| --------------|:-------------------------------------------------------------:|
| **g** 		| Global match, find all matches								|
| **i** 		| Ignore case													|   
| **m** 		| Multiline. ^ and $ working over multiple lines				|
| **u**  		| Unicode. treat pattern as a sequence of unicode code points	|    


#HSLIDE

#### Working with Regular Expressions

**Methods**

+ exec
+ test
+ match
+ search
+ split
+ replace


#HSLIDE

#### exec

The exec() method executes a search for a match in a specified string. Returns a result array, or null.
```javascript
const re = /quick\s(brown).+?(jumps)/ig;
const result = re.exec('The Quick Brown Fox Jumps Over The Lazy Dog');
```

#HSLIDE

#### test

The test() method executes a search for a match between a regular expression and a specified string. Returns true or false.
```javascript
const str = 'hello world!';
const result = /^hello/.test(str);
console.log(result); // true
```

#HSLIDE

#### match

The match() method retrieves the matches when matching a string against a regular expression. Null if there were no matches.
```javascript
const str = 'For more information, see Chapter 3.4.5.1';
const re = /see (chapter \d+(\.\d)*)/i;
const found = str.match(re);
```

#HSLIDE

#### search

The search() method executes a search for a match between a regular expression and this String object. Returns the position of the match; if not found, -1.
```javascript
const str = "Hello Guys";
const n = str.search(/guys/i); // 6
```

#HSLIDE

#### split

The split() method splits a String object into an array of strings by separating the string into substrings.
Syntax
```javascript
str.split([separator[, limit]])
```
Example
```javascript
const myString = 'Hello World. How are you doing?';
const splits = myString.split(' ', 3); // ["Hello", "World.", "How"]
```

#HSLIDE

#### replace

The replace() method returns a new string with some or all matches of a pattern replaced by a replacement. The pattern can be a string or a RegExp, and the replacement can be a string or a function to be called for each match.
```javascript
const str = 'Twas the night before Xmas...';
const newstr = str.replace(/xmas/i, 'Christmas');
console.log(newstr);  // Twas the night before Christmas...
```
```javascript
const re = /(\w+)\s(\w+)/;
const str = "John Smith";
const newstr = str.replace(re, "$2, $1"); // "Smith, John".
```

#HSLIDE

#### Tools

- [regex101.com](https://regex101.com)
- [regexr.com](http://regexr.com)
- [jex.im/regulex](https://jex.im/regulex/)
- [rexegg.com](http://www.rexegg.com)
- [xregexp](https://www.npmjs.com/package/xregexp)